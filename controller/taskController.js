const Task = require ("../models/task");

module.exports.getIdTasks=(taskId)=>{
		return Task.findById(taskId).then(result=>{
			return result
		})
	}

module.exports.updateTaskStatus =(taskId, newContent) =>{

				return Task.findById(taskId).then((result, error) =>{
					if(error){
						console.log(error)
						return false
					}
						result.status = newContent.status;

						return result.save().then((updatedTask, saveErr) =>{

							if(saveErr){
								console.log(saveErr)
								return false
							}else{

								return updatedTask
							}
						})		
			})
		}
