const express = require ('express');
const router = express.Router();
const taskController = require("../controller/taskController")

//GET request task

router.get("/:id", (req, res)=>{
			
			taskController.getIdTasks(req.params.id).then(resultFromController => res.send(resultFromController));
		})


router.put("/:id/complete", (req, res) => {
			taskController.updateTaskStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
		});



module.exports = router;
